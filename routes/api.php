<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GiftsController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//login
Route::post('/login', [LoginController::class, 'index'])->name('login');
Route::post('/register', [LoginController::class, 'register'])->name('register');

Route::middleware('auth_token')->group(function() {
    //logout
    Route::post('/logout', [LoginController::class, 'logout']);
    
    // Route::middleware(['admin'])->group(function() {
    //     //users
    //     Route::get('/users', [UserController::class, 'index']);
    //     Route::get('/users/{user}', [UserController::class, 'show']);

    //     //gifts admin
        Route::post('/gifts', [GiftsController::class, 'store']);
    //     Route::post('/gifts/stock/{gift}', [GiftsController::class, 'storeStock']);
        Route::put('/gifts/{gift}', [GiftsController::class, 'update']);
        Route::patch('/gifts/{gift}', [GiftsController::class, 'updateDescription']);
        Route::delete('/gifts/{gift}', [GiftsController::class, 'destroy']);
    // });
    // Route::put('/users/{user}', [UserController::class, 'update']);

    //gifts
    Route::get('/gifts', [GiftsController::class, 'index']);
    Route::get('/gifts/{gift}', [GiftsController::class, 'show']);
    Route::post('/gifts/{gift}/redeem', [GiftsController::class, 'redeem']);
    Route::post('/gifts/{redeem}/rating', [GiftsController::class, 'rating']);
});