This is a PHP backend app with Laravel framework (version 8).

## Getting Started

After clone the project into local envireonment, install / update vendor:

```bash
composer install
# or
composer update
```

Then, create the .env from .env.example file and set the database to rolling_glory and generate the app key:

```bash
php artisan key:generate
```

And clear the app cache:

```bash
php artisan config:cache
```

Add into .env file:

```bash
cp .env.example .env
```

I have included the DB schema and data file, but you can use the Faker and Seeder method to test with bigger data sample.
You just have to run migration + seeder:

```bash
php artisan migrate:fresh --seed
```

```bash
php artisan passport:install
```

Import Postman collection:

```
DBTesting.postman_collection.json
and
Testing.postman_environment.json
```

Runner all API
