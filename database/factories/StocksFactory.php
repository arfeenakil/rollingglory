<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StocksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    
    private static $id = 1;
    
    public function definition()
    {
        return [
            'year' => date('Y'),
            'month' => date('m'),
            'gift_id' => self::$id++,
            'stock' => 50
        ];
    }

}
