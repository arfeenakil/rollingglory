<?php

namespace Database\Seeders;

use App\Models\Gifts;
use App\Models\Redeems;
use App\Models\Stocks;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::factory(15)->create();
        Gifts::factory(120)->create();
        Redeems::factory(70)->create();
        Stocks::factory(90)->create();

        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'user']);
    }
}
