<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Stocks extends Pivot
{
    use HasFactory;

    protected $fillable = [
        'year',
        'month',
        'gift_id',
        'stock'
    ];

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public static function getStock($today, $giftId)
    {
        $year = Carbon::parse($today)->format('Y');
        $month = Carbon::parse($today)->format('m');

        $stock = Gifts::leftJoin('stock', 'gifts.id', 'stock.gift_id')
        ->leftJoin('redeem', 'gifts.id', 'redeem.gift_id')
        ->where('stock.year', $year)
        ->where('stock.month', $month)
        ->where('gifts.id', $giftId)
        ->selectRaw("
            gifts.id,
            gifts.name,
            stock.stock initial_stock,
            stock.stock - ifnull((select sum(redeem.qty) from redeem where gift_id = gifts.id and year(date) = stock.year and month(date) = stock.month), 0) stock
        ")
        ->groupBy('gifts.id')
        ->first();

        return $stock;
    }
}
