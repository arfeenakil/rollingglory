<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGiftsRequest;
use App\Http\Requests\StoreRedeemsRequest;
use App\Http\Requests\UpdateGiftsRequest;
use App\Http\Resources\GiftsResource;
use App\Http\Resources\RedeemResource;
use App\Models\Gifts;
use App\Models\Redeems;
use App\Models\Stocks;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $year = Carbon::now()->format('Y');
            $month = Carbon::now()->format('m');
            
            $gifts = Gifts::leftJoin('redeem', 'gifts.id', 'redeem.gift_id')
            ->leftJoin('stock', 'gifts.id', 'stock.gift_id')
            ->select('gifts.*')
            ->selectRaw("
                floor((avg(rating)) * 2 + 0.5) / 2 rating,
                stock.stock - ifnull((select sum(redeem.qty) from redeem where gift_id = gifts.id and year(date) = stock.year and month(date) = stock.month), 0) stock
            ")
            ->where('stock.year', $year)
            ->where('stock.month', $month)
            ->groupBy('gifts.id');

            $isSort = $request->sort ?? false;
            if($isSort) {
                if($request->sort == 'asc') {
                    $gifts->orderBy('created_at', 'asc')->orderBy('rating', 'asc');
                } else {
                    $gifts->orderBy('created_at', 'desc')->orderBy('rating', 'desc');
                }
            }

            $isRating = $request->rating ?? false;
            if($isRating) {
                $gifts->havingRaw("rating >= $isRating");
            }

            $isStockAvailable = $request->is_stock_available ?? false;
            if($isStockAvailable) {
                $gifts->havingRaw("stock > 0");
            }

            $gifts = $gifts->paginate(10);
            
            return GiftsResource::collection($gifts);
        } catch(\Exception $e) {
            return response()->json([
                'result' => false,
                'data' => [
                    'message' => $e->getMessage()
                ],
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreGiftsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGiftsRequest $request)
    {
        try {
            $gift = Gifts::create($request->all());

            $message = 'Success input new gift';

            return new GiftsResource($gift);
        } catch(\Exception $e) {
            DB::rollback();

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gifts  $gifts
     * @return \Illuminate\Http\Response
     */
    public function show(Gifts $gifts)
    {
        try {
            $giftId = $gifts->id;
            $giftRating = Gifts::join('redeem', 'gifts.id', 'redeem.gift_id')
            ->selectRaw("floor((avg(rating)) * 2 + 0.5) / 2 rating")
            ->where('gifts.id', $giftId)
            ->first();
            $gifts->rating = $giftRating->rating;

            return new GiftsResource($gifts);
        } catch(\Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gifts  $gifts
     * @return \Illuminate\Http\Response
     */
    public function edit(Gifts $gifts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateGiftsRequest  $request
     * @param  \App\Models\Gifts  $gifts
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGiftsRequest $request, Gifts $gifts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gifts  $gifts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gifts $gifts)
    {
        $gift->delete();
        return response()->json(['success' => 'Gift deleted successfully']);
    }

    public function redeem(StoreRedeemsRequest $request, Gifts $gift)
    {
        try {
            $qty = $request->qty;
            $userId = auth()->guard('api')->user()->id;
            $giftId = $gift->id;
            $giftName = $gift->name;
            $today = Carbon::now()->format('Y-m-d');

            $stock = Stocks::getStock($today, $giftId);
            if($stock->stock < $qty) {
                $message = "Insufficient stock, stock available for item $giftName is $stock->stock";

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            $redeem = Redeems::create([
                'user_id' => $userId,
                'gift_id' => $giftId,
                'qty' => $qty,
                'date' => $today
            ]);

            $message = 'Success redeem gift';

            return new RedeemResource($redeem);
        } catch(\Exception $e) {
            
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function rating(Request $request, Redeem $redeem)
    {
        try {
            $validator = Validator::make($request->all(), [
                'rating' => 'required|numeric|min:1|max:5'
            ]);

            if($validator->fails()) {
                $message = 'Please complete or check data';
                $error = $validator->messages();

                return response()->json([
                    'result' => false,
                    'message' => $message,
                    'error' => $error
                ]);
            }
            
            $redeemId = $redeem->id;
            $redeemUserId = $redeem->user_id;
            $userId = auth()->guard('api')->user()->id;
            $rating = $request->rating;
            if($redeemUserId != $userId) {
                $message = 'Unauthorized user';

                return response()->json([
                    'result' => false,
                    'message' => $message
                ]);
            }

            $redeem = Redeems::where('id', $redeemId)
            ->where('user_id', $userId)
            ->update([
                'rating' => $rating
            ]);

            $message = 'Success submit rating';

            return new RedeemResource($redeem);
        } catch(\Exception $e) {
            Log::debug($e->getMessage() . ' in ' . $e->getFile() . ' line ' . $e->getLine());

            return response()->json([
                'result' => false,
                'message' => 'Something went wrong'
            ]);
        }
    }
}
