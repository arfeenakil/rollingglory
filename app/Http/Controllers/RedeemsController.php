<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRedeemsRequest;
use App\Http\Requests\UpdateRedeemsRequest;
use App\Models\Redeems;

class RedeemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRedeemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRedeemsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Redeems  $redeems
     * @return \Illuminate\Http\Response
     */
    public function show(Redeems $redeems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Redeems  $redeems
     * @return \Illuminate\Http\Response
     */
    public function edit(Redeems $redeems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRedeemsRequest  $request
     * @param  \App\Models\Redeems  $redeems
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRedeemsRequest $request, Redeems $redeems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Redeems  $redeems
     * @return \Illuminate\Http\Response
     */
    public function destroy(Redeems $redeems)
    {
        //
    }
}
